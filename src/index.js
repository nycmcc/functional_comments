import React from 'react';
import ReactDOM from 'react-dom';

import faker from 'faker';

import CommentDetail from './CommentDetail';
import ApprovalCard from './ApprovalCard';

const App = () => {
  return (
    <div className="ui container comments">
    <ApprovalCard>
      <div>
        <h4>
          Warning!
        </h4>
        Are you sure you want to do this?
      </div>
    </ApprovalCard>
    {/* <ApprovalCard>
      <CommentDetail who="Jane"/>
    </ApprovalCard> */}
      <ApprovalCard>
        {/* 
          CommentDetail: All reference to "author" properties (line 12 in CommentDetail.js)  
          See example below (lines 11 and 12) => author is a defined property, 
          but "who" as property is undefined
        */}
        {/* 
          CommentDetail: Would refer to this as a prop as well because we are providing some configuration to the approval
          card component.
        */}
        <CommentDetail author="Sam" timeAgo="Today at 4:45PM" content="Nice blog post!" avatar={faker.image.avatar()} />
      </ApprovalCard>
      <ApprovalCard>
        <CommentDetail author="Dave" timeAgo="Today at 6PM" content="Sweet!" avatar={faker.image.avatar()}/>
      </ApprovalCard>
      <ApprovalCard>
        <CommentDetail author="Max" timeAgo="Today at 10:25AM" content="Awesome!" avatar={faker.image.avatar()}/>
      </ApprovalCard>
      <ApprovalCard>
        <CommentDetail author="Alex" timeAgo="Today at 12AM" content="Cool, dude!" avatar={faker.image.avatar()}/>
      </ApprovalCard>
    </div>
  );
};

ReactDOM.render(<App />, document.querySelector('#root'));